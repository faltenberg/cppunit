#include "cppunit.hpp"

#include "selftest.hpp"
#include "template.hpp"

using namespace cppunit;


int main() {
  TestResult result;
  result += SelfTest::allTests(VERBOSE);
  result += Template::allTests(SPARSE);
  result.print();
  return 0;
}
