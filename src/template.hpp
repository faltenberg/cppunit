#pragma once


/**
 * Example
 * -------
 *
 * ```cpp
 * #include "cppunit.hpp"
 * #include "template.hpp"
 *
 * using namespace cppunit;
 *
 * int main() {
 *   TestResult result;
 *   result += Template::allTests(VERBOSE);
 *   result.print();
 * }
 * ```
 *
 * Expand
 * ------
 *
 * 1. change filename and class name if desired
 * 2. for convinience each test suite shall have an `allTests()` function
 * 3. write test cases and don't forget to add them inside the constructor
 *    (use macros for more expressibility)
 * 4. write a `main()` function
 */


#include "cppunit.hpp"

using namespace cppunit;


class Template : public TestSuite {
  static TestResult simpleTest() {
    int value = 42;
    return assertEqualInt(value, 42);
  }


  static TestResult testWithMacros() {
    TestResult result = {};         // NECESSARY, macros expect it
    INFO("test with macros");       // simply prints a message
    TEST(assertEqualInt(13, 42));   // result += assertEqualInt(13, 42);
    SKIP(assertEqualInt(42, 42));   // result = result
    FAIL("intended to fail");       // result += false
    ABORT(assertEqualInt(42, 42));  // like TEST(assertEqualInt(42, 42)) BUT
    ABORT(assertEqualInt(13, 42));  // will exit testWithMacros() if assertion fails
    TEST(assertEqualInt(42, 42));   // won't be reached
    return result;                  // result has 3 failed tests out of 4
  }


public:
  Template(PrintLevel verbosity) : TestSuite(__FILE__, "Test Suite Template") {
    setVerbosity(verbosity);
    addTest(&simpleTest,     "simpleTest");
    addTest(&testWithMacros, "testWithMacros");
  }


  static TestResult allTests(PrintLevel verbosity=VERBOSE) {
    return Template(verbosity).run();
  }
};
