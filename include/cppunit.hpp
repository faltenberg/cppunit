#pragma once

#include <cstdio>
#include <cstring>
#include <cstdarg>
#include <cfloat>
#include <cmath>
#include <vector>


namespace cppunit {


/******************************************** INTERNAL *******************************************/

#define FPRECISION 0.00001f
#define DPRECISION 0.00000001


/**
 * Compares floats and returns true if their relative distance is within some limit.
 * Special cases: -0.0f==0.0f, nan!=nan.
 * Source: https://www.floating-point-gui.de/errors/comparison/
 */
static bool cmpFloat(float a, float b) {
  float absA = a < 0.0f ? -a : a;
  float absB = b < 0.0f ? -b : b;
  float diff = fabs(a - b);

  if (a == b) {
    return true;  // shortcut, covers infinities and -0.0f
  } else if (a == 0.0f || b == 0.0f || diff < FLT_MIN) {
    return diff < (FPRECISION * FLT_MIN);  // a or b close to zero, relative error thus meaningless
  } else {
    return (diff / fmin(absA+absB, FLT_MAX)) < FPRECISION;  // return relative error
  }
}

static bool cmpDouble(double a, double b) {
  double absA = a < 0.0 ? -a : a;
  double absB = b < 0.0 ? -b : b;
  double diff = fabs(a - b);

  if (a == b) {
    return true;  // shortcut, covers infinities and -0.0
  } else if (a == 0.0 || b == 0.0 || diff < DBL_MIN) {
    return diff < (DPRECISION * DBL_MIN);  // a or b close to zero, relative error thus meaningless
  } else {
    return (diff / fmin(absA+absB, DBL_MAX)) < DPRECISION;  // return relative error
  }
}


/**
 * Returns -1 if strings are equal otherwise the index at which they differ.
 */
static int cmpString(const char* a, const char* b) {
  int index = -1;
  bool equal = true;

  while (equal) {
    equal = *a == *b;
    index++;
    if (*a == '\0' || *b == '\0') break;
    a++; b++;
  }

  return equal ? -1 : index;
}


/**
 * Returns -1 if the memory blocks are equal otherwise the index at which they differ.
 */
static int cmpMemory(const void* a, const void* b, size_t length) {
  int index;
  bool equal = true;

  for (index = 0; equal && index < length; index++) {
    equal = ((unsigned char*) a)[index] == ((unsigned char*) b)[index];
  }
  index--;  // compensate the increment on loop exit

  return equal ? -1 : index;
}


static const char* __tostring(bool b) {
  return b ? "true" : "false";
}


static const char* __tostring(char c) {
  static const char* table[] = {
      "'\\0'", "'SOH'", "'STX'", "'ETX'", "'EOT'", "'ENQ'", "'ACK'", "'BEL'",
      "'\\b'", "'\\t'", "'\\n'", "'\\v'", "'\\f'", "'\\r'", "'SO'",  "'SI'",
      "'DLE'", "'DC1'", "'DC2'", "'DC3'", "'DC4'", "'NAK'", "'SYN'", "'ETB'",
      "'CAN'", "'EM'",  "'SUB'", "'ESC'", "'FS'",  "'GS'",  "'RS'",  "'US'",
      "' '",   "'!'",   "'\"'",  "'#'",   "'$'",   "'%'",   "'&'",   "'''",
      "'('",   "')'",   "'*'",   "'+'",   "'´'",   "'-'",   "'.'",   "'/'",
      "'0'",   "'1'",   "'2'",   "'3'",   "'4'",   "'5'",   "'6'",   "'7'",
      "'8'",   "'9'",   "':'",   "';'",   "'<'",   "'='",   "'>'",   "'?'",
      "'@'",   "'A'",   "'B'",   "'C'",   "'D'",   "'E'",   "'F'",   "'G'",
      "'H'",   "'I'",   "'J'",   "'K'",   "'L'",   "'M'",   "'N'",   "'O'",
      "'P'",   "'Q'",   "'R'",   "'S'",   "'T'",   "'U'",   "'V'",   "'W'",
      "'X'",   "'Y'",   "'Z'",   "'['",   "'\\'",  "']'",   "'^'",   "'_'",
      "'`'",   "'a'",   "'b'",   "'c'",   "'d'",   "'e'",   "'f'",   "'g'",
      "'h'",   "'i'",   "'j'",   "'k'",   "'l'",   "'m'",   "'n'",   "'o'",
      "'p'",   "'q'",   "'r'",   "'s'",   "'t'",   "'u'",   "'v'",   "'w'",
      "'x'",   "'y'",   "'z'",   "'{'",   "'|'",   "'}'",   "'~'",   "'DEL'",
  };

  return table[(unsigned char) c];
}


#define RED "\x1B[31m"
#define GRN "\x1B[32m"
#define YEL "\x1B[33m"
#define BLU "\x1B[34m"
#define MAG "\x1B[35m"
#define CYN "\x1B[36m"
#define WHT "\x1B[37m"
#define RST "\x1B[0m"


enum PrintLevel {
  SILENT, SUMMARY, SPARSE, VERBOSE
};


static PrintLevel __printLevel = VERBOSE;


void printVerbose(const char* format, ...) {
  if (__printLevel >= VERBOSE) {
    va_list args;
    va_start(args, format);
    vprintf(format, args);
    va_end(args);
  }
}


void printSparse(const char* format, ...) {
  if (__printLevel >= SPARSE) {
    va_list args;
    va_start(args, format);
    vprintf(format, args);
    va_end(args);
  }
}


void printSummary(const char* format, ...) {
  if (__printLevel >= SUMMARY) {
    va_list args;
    va_start(args, format);
    vprintf(format, args);
    va_end(args);
  }
}


void printAlways(const char* format, ...) {
  va_list args;
  va_start(args, format);
  vprintf(format, args);
  va_end(args);
}


/******************************************** CLASSES ********************************************/


struct TestResult {
  int failedTests;
  int totalTests;

  TestResult() : failedTests(0), totalTests(0) {}

  TestResult(bool b) : failedTests(b ? 0 : 1), totalTests(1) {}

  TestResult& operator+=(const TestResult& result) {
    failedTests += result.failedTests;
    totalTests += result.totalTests;
    return *this;
  }

  void print() {
    float passedRatio = failedTests / (totalTests + FPRECISION);
    passedRatio = 100.0f * (1.0f - passedRatio);
    printSummary(BLU "[Result]" RST " %d of %d tests failed (%.2f%% passed)\n",
                 failedTests, totalTests, passedRatio);
  }
};


class TestSuite {
private:
  typedef TestResult (*TEST_FN)();

  struct Test {
    const char* name;
    TEST_FN     fn;
  };

public:
  TestSuite(const char* name, const char* desc="") : _name(name), _desc(desc) {}

  void addTest(TEST_FN fn, const char* name) {
    _tests.push_back((Test) { name, fn });
  }

  TestResult run() {
    PrintLevel temp = __printLevel;
    __printLevel = _verbosity;

    TestResult result;
    printSummary(BLU "[%s]" RST " %s\n", _name, _desc);

    for (auto test : _tests) {
      if (__printLevel < VERBOSE) {
        printSparse("%s ", test.name);
        for(int i = strlen(test.name); i < 45; i++) printSparse(".");
        printSparse("... ");
      } else {
        printSparse("%s ... \n", test.name);
      }

      TestResult r = test.fn();

      if (__printLevel < VERBOSE) {
        if (r.failedTests == 0) {
          printSparse(GRN "OK\n" RST);
        } else {
          printSparse(RED "ERROR:" RST " %d of %d tests failed\n", r.failedTests, r.totalTests);
        }
      }

      result.failedTests += r.failedTests;
      result.totalTests += r.totalTests;
    }

    float passedRatio = result.failedTests / (result.totalTests + FPRECISION);
    passedRatio = 100.0f * (1.0f - passedRatio);
    printSummary(BLU "[%s]" RST " %d of %d tests failed (%.2f%% passed)\n",
                _name, result.failedTests, result.totalTests, passedRatio);
    printSummary("\n");

    __printLevel = temp;
    return result;
  }

protected:
  void setVerbosity(PrintLevel verbosity) {
    _verbosity = verbosity;
  }

private:
  const char*       _name;
  const char*       _desc;
  std::vector<Test> _tests;
  PrintLevel        _verbosity;
};


/****************************************** ASSERTIONS *******************************************/


#define __PROMPT " %s:%-3d ... "

#define __ABORT printVerbose(__PROMPT RED "ABORT\n" RST, __FILE__, __LINE__); return result;
#define ABORT(a) { bool __r = (a); result += __r; if (!__r) { __ABORT } }

#define TEST(a) result += (a);

#define SKIP(a) printVerbose(__PROMPT YEL "SKIPPED\n" RST, __FILE__, __LINE__);

#define FAIL(m) result += false; printVerbose(__PROMPT RED "FAIL: " RST "%s\n", __FILE__, __LINE__, m);

#define INFO(m) printVerbose(__PROMPT BLU "INFO: " RST "%s\n", __FILE__, __LINE__, m);


template<typename C, typename P>
bool __validate(const char* file, int line, C check, P printDescription) {
  printVerbose(__PROMPT, file, line);

  if (check()) {
    printVerbose(GRN "OK\n" RST);
    return true;
  } else {
    printVerbose(RED "ERROR: " RST);  printDescription();  printVerbose("\n");
    return false;
  }
}


#define assertFalse(val) __assertFalse(__FILE__, __LINE__, val)
bool __assertFalse(const char* file, int line, bool cond) {
  auto test = [=]() { return cond == false; };
  auto desc = [=]() {
       printVerbose("expected [%s]", __tostring(false));
  };
  return __validate(file, line, test, desc);
}


#define assertTrue(val) __assertTrue(__FILE__, __LINE__, val)
bool __assertTrue(const char* file, int line, bool cond) {
  auto test = [=]() { return cond == true; };
  auto desc = [=]() {
       printVerbose("expected [%s]", __tostring(true));
  };
  return __validate(file, line, test, desc);
}


#define assertEqualBool(val, exp) __assertEqualBool(__FILE__, __LINE__, val, exp)
bool __assertEqualBool(const char* file, int line, bool value, bool expected) {
  auto test = [=]() { return value == expected; };
  auto desc = [=]() {
       printVerbose("expected [%s] == [%s]", __tostring(value), __tostring(expected));
  };
  return __validate(file, line, test, desc);
}


#define assertNotEqualBool(val, exp) __assertNotEqualBool(__FILE__, __LINE__, val, exp)
bool __assertNotEqualBool(const char* file, int line, bool value, bool expected) {
  auto test = [=]() { return value != expected; };
  auto desc = [=]() {
       printVerbose("expected [%s] != [%s]", __tostring(value), __tostring(expected));
  };
  return __validate(file, line, test, desc);
}


#define assertEqualInt(val, exp) __assertEqualInt(__FILE__, __LINE__, val, exp)
bool __assertEqualInt(const char* file, int line, int value, int expected) {
  auto test = [=]() { return value == expected; };
  auto desc = [=]() {
       printVerbose("expected [%d] == [%d]", value, expected);
  };
  return __validate(file, line, test, desc);
}


#define assertNotEqualInt(val, exp) __assertNotEqualInt(__FILE__, __LINE__, val, exp)
bool __assertNotEqualInt(const char* file, int line, int value, int expected) {
  auto test = [=]() { return value != expected; };
  auto desc = [=]() {
       printVerbose("expected [%d] != [%d]", value, expected);
  };
  return __validate(file, line, test, desc);
}


#define assertEqualChar(val, exp) __assertEqualChar(__FILE__, __LINE__, val, exp)
bool __assertEqualChar(const char* file, int line, char value, char expected) {
  auto test = [=]() { return value == expected; };
  auto desc = [=]() {
       printVerbose("expected [%s] == [%s]", __tostring(value), __tostring(expected));
  };
  return __validate(file, line, test, desc);
}


#define assertNotEqualChar(val, exp) __assertNotEqualChar(__FILE__, __LINE__, val, exp)
bool __assertNotEqualChar(const char* file, int line, char value, char expected) {
  auto test = [=]() { return value != expected; };
  auto desc = [=]() {
       printVerbose("expected [%s] != [%s]", __tostring(value), __tostring(expected));
  };
  return __validate(file, line, test, desc);
}


#define assertEqualSize(val, exp) __assertEqualSize(__FILE__, __LINE__, val, exp)
bool __assertEqualSize(const char* file, int line, size_t value, size_t expected) {
  auto test = [=]() { return value == expected; };
  auto desc = [=]() {
       printVerbose("expected [%lu] == [%lu]", value, expected);
  };
  return __validate(file, line, test, desc);
}


#define assertNotEqualSize(val, exp) __assertNotEqualSize(__FILE__, __LINE__, val, exp)
bool __assertNotEqualSize(const char* file, int line, size_t value, size_t expected) {
  auto test = [=]() { return value != expected; };
  auto desc = [=]() {
       printVerbose("expected [%lu] != [%lu]", value, expected);
  };
  return __validate(file, line, test, desc);
}


#define assertEqualFloat(val, exp) __assertEqualFloat(__FILE__, __LINE__, val, exp)
bool __assertEqualFloat(const char* file, int line, float value, float expected) {
  auto test = [=]() { return cmpFloat(value, expected); };
  auto desc = [=]() {
       printVerbose("expected [%e] == [%e]", value, expected);
  };
  return __validate(file, line, test, desc);
}


#define assertNotEqualFloat(val, exp) __assertNotEqualFloat(__FILE__, __LINE__, val, exp)
bool __assertNotEqualFloat(const char* file, int line, float value, float expected) {
  auto test = [=]() { return !cmpFloat(value, expected); };
  auto desc = [=]() {
       printVerbose("expected [%e] != [%e]", value, expected);
  };
  return __validate(file, line, test, desc);
}


#define assertEqualDouble(val, exp) __assertEqualDouble(__FILE__, __LINE__, val, exp)
bool __assertEqualDouble(const char* file, int line, double value, double expected) {
  auto test = [=]() { return cmpDouble(value, expected); };
  auto desc = [=]() {
       printVerbose("expected [%e] == [%e]", value, expected);
  };
  return __validate(file, line, test, desc);
}


#define assertNotEqualDouble(val, exp) __assertNotEqualDouble(__FILE__, __LINE__, val, exp)
bool __assertNotEqualDouble(const char* file, int line, double value, double expected) {
  auto test = [=]() { return !cmpDouble(value, expected); };
  auto desc = [=]() {
       printVerbose("expected [%e] != [%e]", value, expected);
  };
  return __validate(file, line, test, desc);
}


#define assertEqualString(str, exp) __assertEqualString(__FILE__, __LINE__, str, exp)
bool __assertEqualString(const char* file, int line, const char* string, const char* expected) {
  int index;
  auto test = [&]() {
      index = cmpString(string, expected);
      return index == -1;
  };
  auto desc = [&]() {
      printVerbose("in \"%.5s\"%s[%d] expected [%s] == [%s]",
                   string, (strlen(string) > 5 ? "~" : ""), index,
                   __tostring(string[index]), __tostring(expected[index]));
  };
  return __validate(file, line, test, desc);
}


#define assertNotEqualString(str, exp) __assertNotEqualString(__FILE__, __LINE__, str, exp)
bool __assertNotEqualString(const char* file, int line, const char* string, const char* expected) {
  int index;
  auto test = [&]() {
      index = cmpString(string, expected);
      return index != -1;
  };
  auto desc = [&]() {
      printVerbose("expected [\"%.5s\"%s] != [\"%.5s\"%s]",
                   string, (strlen(string) > 5 ? "~" : ""), expected,
                   (strlen(expected) > 5 ? "~" : ""));
  };
  return __validate(file, line, test, desc);
}


#define assertNull(ptr) __assertNull(__FILE__, __LINE__, ptr)
bool __assertNull(const char* file, int line, const void* pointer) {
  auto test = [=]() { return pointer == nullptr; };
  auto desc = [=]() {
      printVerbose("expected [%p] == [%p]", pointer, nullptr);
  };
  return __validate(file, line, test, desc);
}


#define assertNotNull(ptr) __assertNotNull(__FILE__, __LINE__, ptr)
bool __assertNotNull(const char* file, int line, const void* pointer) {
  auto test = [=]() { return pointer != nullptr; };
  auto desc = [=]() {
      printVerbose("expected [%p] != [%p]", pointer, nullptr);
  };
  return __validate(file, line, test, desc);
}


#define assertSame(ptr, exp) __assertSame(__FILE__, __LINE__, ptr, exp)
bool __assertSame(const char* file, int line, const void* pointer, const void* expected) {
  auto test = [=]() { return pointer == expected; };
  auto desc = [=]() {
      printVerbose("expected [%p] == [%p]", pointer, expected);
  };
  return __validate(file, line, test, desc);
}


#define assertNotSame(ptr, exp) __assertNotSame(__FILE__, __LINE__, ptr, exp)
bool __assertNotSame(const char* file, int line, const void* pointer, const void* expected) {
  auto test = [=]() { return pointer != expected; };
  auto desc = [=]() {
      printVerbose("expected [%p] != [%p]", pointer, expected);
  };
  return __validate(file, line, test, desc);
}


#define assertEqualMemory(ptr, exp, len) __assertEqualMemory(__FILE__, __LINE__, ptr, exp, len)
bool __assertEqualMemory(const char* file, int line,
                         const void* pointer, const void* expected, size_t length) {
  int index;
  auto test = [&]() {
      index = cmpMemory(pointer, expected, length);
      return index == -1;
  };
  auto desc = [&]() {
      auto p = (const unsigned char*) pointer;
      auto e = (const unsigned char*) expected;
      printVerbose("at (%p)[%d] expected [0x%02x] == [0x%02x]", pointer, index, p[index], e[index]);
  };
  return __validate(file, line, test, desc);
}


};  // namespace
