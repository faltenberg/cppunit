CPPUnit Test API
================

This API provides a simple way to write unit tests with as little code as necessary. It consists
of one header file only.


Project Structure
-----------------

- *include/* contains all header files
- *src/* contains the the selftest and a template for test suites
- *bin/* contains the executable selftest


Building CPPUnit
----------------

Use `make run` to execute a selftest and use `make clean` to delete all built files.


Adding CPPUnit to Your Project
------------------------------

Add the *include/cppunit.hpp* header file to the search path of your compiler. Copy the
`src/template.hpp` test suite template to your project and rename it if you wish. Follow the
instructions written there.


Writing a Test Suite
--------------------

Each test suite should reside in a separate *.hpp* file and each test case function should be
declared as `static`. This way no name collisions should occur. To call the test suite from a
`main()` function it is adviced to have an `alltests()` function inside the test suite. You can
then compile all your test suites together with a `main()` function to create an executable test.

```cpp
// inside template.hpp
#include "cppunit.hpp"
using namespace cppunit;

class Template : public TestSuite {
  static TestResult testCase() {
    TestResult result;
    result += assertEqualInt(42, 42);
    TEST(assertEqualInt(42, 42));  // does exactly the same as the line above
    return result;
  }

public:
  Template(PrintLevel verbosity) : TestSuite(__FILE__, "Test Suite Template") {
    setVerbosity(verbosity);
    addTest(&testCase, "testCase");
  }

  static TestResult allTests(PrintLevel verbosity=VERBOSE) {
    return Template(verbosity).run();
  }
};

// inside main.cpp
#include "cppunit.hpp"
#include "template.hpp"
using namespace cppunit;

int main() {
  TestResult result;
  result += Template::allTests(VERBOSE);
  result.print();
  return 0;
}
```


Modifying Verbosity
-------------------

By passing a `PrintLevel` to `alltests()` (which can be propagated to `run()` via
`TestSuite::setVerbosity()`) of a test suite one can apply the level of detail for the output.
If the level is set to `VERBOSE` then the test suite will print the result of every assertion.
By passing  `SPARSE` the summary of each test case is printed. `SUMMARY` will print only the result
of each test suite. `SILENT` will supress any output. When developping on a new test suite the
`VERBOSE` level is suited best. Once all test cases pass, one can set the level to `SPARSE` or
even to squash the output. If a test suite has many test cases the output can be reduced with the
`SUMMARY` level even further. `SILENT` is best suited if one is only interested in the test result
that is for instance used in some script.


Writing Custom Assertions
-------------------------

CPPUnit comes with a bunch of assert functions.

```cpp
bool assertFalse(bool cond);
bool assertTrue(bool cond);

bool assertEqualBool(bool value, bool expected);
bool assertNotEqualBool(bool value, bool expected);

bool assertEqualInt(int value, int expected);
bool assertNotEqualInt(int value, int expected);

bool assertEqualChar(char value, char expected);
bool assertNotEqualChar(char value, char expected);

bool assertEqualSize(size_t value, size_t expected);
bool assertNotEqualSize(size_t value, size_t expected);

bool assertEqualFloat(float value, float expected);
bool assertNotEqualFloat(float value, float expected);

bool assertEqualDouble(double value, double expected);
bool assertNotEqualDouble(double value, double expected);

bool assertEqualString(const char* string, const char* expected);
bool assertNotEqualString(const char* string, const char* expected);

bool assertNull(const void* pointer);
bool assertNotNull(const void* pointer);

bool assertSame(const void* pointer, const void* expected);
bool assertNotSame(const void* pointer, const void* expected);

bool assertEqualMemory(const void* pointer, const void* expected, size_t length);
```

If they don't suffice your needs you can simply write your own assert function.

```cpp
#include "cppunit.h"

#define assertEqual(val, exp) __assertEqual(__FILE__, __LINE__, val, exp)
bool __assertEqualInt(const char* file, int line, int value, int expected) {
  auto test = [=]() { return value == expected; };
  auto desc = [=]() {
       printVerbose("expected [%d] == [%d]", value, expected);
  };
  return __validate(file, line, test, desc);
}
```

To imitate the output behavior of CPPUnit it is adviced to use the provided `printVerbose()`
function instead of `printf()` and call `__validate()`.


Using Macros
------------

For convinience and for better expressibility CPPUnit comes with a couple of macros to call your
assertions with. All you need to do is to declare a `TestResult` variable.

The `TEST()` macro is a simple wrapper around an assert function which will automatically store
the result in the `TestResult` variable.

If you want to abort a test case on a strong precondition you can use the `ABORT()` macro. It works
just like `TEST()` but exits the surrounding function if the assertion failed.

You can skip a test with the `SKIP()` macro. The `INFO()` macro can be used to print a message,
whereas the `FAIL()` will print a message and add a failed test to the result.

```cpp
static TestResult example() {
  TestResult result;              // NECESSARY, macros expect it, do NOT change
  INFO("test with macros");       // simply prints a message
  TEST(assertEqualInt(13, 42));   // result += assertEqualInt(13, 42);
  SKIP(assertEqualInt(42, 42));   // result = result
  FAIL("intended to fail");       // result += false
  ABORT(assertEqualInt(42, 42));  // like TEST(assertEqualInt(42, 42)) BUT
  ABORT(assertEqualInt(13, 42));  // will exit testWithMacros() if assertion fails
  TEST(assertEqualInt(42, 42));   // won't be reached
  return result;                  // result has 3 failed tests out of 4
}
```

All macros are effected by the verbosity level and are set to `VERBOSE`. That cannot be modified.


Data Structures and Functions
-----------------------------

The central point of CPPUnit is the `TestSuite` class that stores and executes all added test
cases. Results are collected in the `TestResult` structure and propagated to the caller. The whole
API is nested within the namespace `cppunit` and therefore it can be included everywhere without
the need for a library to avoid name collisions and doubly defined functions.

```cpp
enum PrintLevel {
  SILENT, SUMMARY, SPARSE, VERBOSE
};

struct TestResult {
  int failedTests;
  int totalTests;

  TestResult& operator+=(const TestResult& result);  // unites two test results

  TestResult& operator+=(bool result);               // applies a boolean to the result

  void print();                                      // prints the result
};

class TestSuite {
  typedef TestResult (*TEST_FN)();

  TestSuite(const char* name, const char* desc="");  // create new test suite

  void addTest(TEST_FN fn, const char* name);        // adds a test case to the suite

  TestResult run();                                  // executes all tests in the suite

protected:
  void setVerbosity(PrintLevel verbosity);           // set the verbosity for the test suite
};

void printVerbose(const char* format, ...);  // print when level is set to VERBOSE or higher

void printSparse(const char* format, ...);   // print if level is set to SPARSE or higher

void printSummary(const char* format, ...);  // print if level is set to SUMMARY or higher

void printAlways(const char* format, ...);   // ignore the level and print
```
